// ----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2017 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include <string.h>
#include "jclient.h"


Jclient::Jclient (const char *jname, const char *jserv, int nchan) :
    A_thread ("Jclient"),
    _jack_client (0),
    _active (false),
    _jname (0),
    _nchan (nchan),
    _ipg0 (1),	
    _ipg1 (1),	
    _dipg (0)
{
    init_jack (jname, jserv);   
}


Jclient::~Jclient (void)
{
    if (_jack_client) close_jack ();
}


void Jclient::init_jack (const char *jname, const char *jserv)
{
    int            i;
    char           s [16];
    jack_status_t  stat;
    int            opts;

    opts = JackNoStartServer;
    if (jserv) opts |= JackServerName;
    if ((_jack_client = jack_client_open (jname, (jack_options_t) opts, &stat, jserv)) == 0)
    {
        fprintf (stderr, "Can't connect to JACK\n");
        exit (1);
    }
    jack_set_process_callback (_jack_client, jack_static_process, (void *) this);
    jack_on_shutdown (_jack_client, jack_static_shutdown, (void *) this);
    if (jack_activate (_jack_client))
    {
        fprintf(stderr, "Can't activate JACK.\n");
        exit (1);
    }
    _jname = jack_get_client_name (_jack_client);
    _fsamp = jack_get_sample_rate (_jack_client);
    _fragm = _fsamp / 30;
    _nsamp = 0;
    _dplimit.init (_fsamp, _nchan, 0.0f, 0.01f);

    for (i = 0; i < _nchan; i++)
    {
	sprintf (s, "in_%d", i + 1);
        _inpports [i] = jack_port_register (_jack_client, s, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
	sprintf (s, "out_%d", i + 1);
        _outports [i] = jack_port_register (_jack_client, s, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    }
    _active = true;
}


void Jclient::close_jack ()
{
    jack_deactivate (_jack_client);
    jack_client_close (_jack_client);
}


void Jclient::jack_static_shutdown (void *arg)
{
    ((Jclient *) arg)->jack_shutdown ();
}


int Jclient::jack_static_process (jack_nframes_t nframes, void *arg)
{
    return ((Jclient *) arg)->jack_process (nframes);
}


void Jclient::jack_shutdown (void)
{
    send_event (EV_EXIT, 1);
}


int Jclient::jack_process (int frames)
{
    int   i, j, k;
    float d, g;
    float *p, *q;
    float *inp [MAXCHAN];
    float *out [MAXCHAN];

    if (!_active) return 0;

    for (i = 0; i < _nchan; i++)
    {
	inp [i] = (float *) jack_port_get_buffer (_inpports [i], frames);
	out [i] = (float *) jack_port_get_buffer (_outports [i], frames);
    }

    while (frames)
    {
	if (!_nsamp)
	{
	    _dipg = (_ipg0 - _ipg1);
	    if (fabsf (_dipg) < 1e-6f)
	    {
 	        _ipg1 = _ipg0;
                _dipg = 0;
	    }
	    else _dipg /= _fragm;
	    _nsamp = _fragm;
	}

	k = (_nsamp < frames) ? _nsamp : frames;
	g = _ipg1;
	d = _dipg;
	for (i = 0; i < _nchan; i++)
	{
	    p = inp [i];
	    q = out [i];
	    g = _ipg1;
            for (j = 0; j < k; j++) 
	    {
	        g += d;
	        q [j] = g * p [j];
	    }
	}
	_ipg1 = g;

	_dplimit.process (k, out);
	for (i = 0; i < _nchan; i++)
	{
	    inp [i] += k;
	    out [i] += k;
	}
	frames -= k;
	_nsamp -= k;
    }



    return 0;
}


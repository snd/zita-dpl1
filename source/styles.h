// ----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2017 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __STYLES_H
#define __STYLES_H

#include <clxclient.h>
#include "rotary.h"


enum
{
    C_MAIN_BG, C_MAIN_FG, C_DISP_BG,
    C_TEXT_BG, C_TEXT_FG,
    C_INPGAIN, C_THRESHD, C_RELTIME,
    NXFTCOLORS
};

enum
{
    F_PARAMS,
    NXFTFONTS
};


extern void styles_init (X_display *disp, X_resman *xrm);
extern void styles_fini (X_display *disp);

extern XftColor  *XftColors [NXFTCOLORS];
extern XftFont   *XftFonts [NXFTFONTS];

extern X_textln_style tstyle1;

extern XImage     *parsect_img;
extern XImage     *redzita_img;
extern XImage     *hmeter0_img;
extern XImage     *hmeter1_img;
extern RotaryGeom  r_ipgain_geom;
extern RotaryGeom  r_thresh_geom;
extern RotaryGeom  r_reltim_geom;


#endif

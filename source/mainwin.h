// ----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2017 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __MAINWIN_H
#define	__MAINWIN_H


#include <clxclient.h>
#include "guiclass.h"
#include "jclient.h"
#include "global.h"


class Mainwin : public A_thread, public X_window, public X_callback
{
public:

    enum { XSIZE = 640, YSIZE = 75 };

    Mainwin (X_rootwin *parent, X_resman *xres, int xp, int yp, Jclient *jclient);
    ~Mainwin (void);
    Mainwin (const Mainwin&);
    Mainwin& operator=(const Mainwin&);

    void stop (void) { _stop = true; }
    int process (void); 

private:

    enum { R_INPGAIN, R_THRESHD, R_RELTIME };

    virtual void thr_main (void) {}

    void handle_time (void);
    void handle_stop (void);
    void handle_event (XEvent *);
    void handle_callb (int type, X_window *W, XEvent *E);
    void expose (XExposeEvent *E);
    void clmesg (XClientMessageEvent *E);
    void redraw (void);
    void init_disp (void);
    void disp_levels (float gmin, float gmax, float peak);
    void print_param (int k);

    Atom            _atom;
    bool            _stop;
    bool            _ambis;
    X_resman       *_xres;
    Jclient        *_jclient;
    int             _xdisp;
    int             _km;
    X_window       *_dispwin;
    Pixmap          _dispmap;
    GC              _dispgct;
    RotaryCtl      *_inpgain;
    RotaryCtl      *_threshd;
    RotaryCtl      *_reltime;
};


#endif
